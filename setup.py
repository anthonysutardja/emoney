#!/usr/bin/env python
from setuptools import setup, find_packages

__version__ = '0.1'

setup(
    name='emoney',
    version=__version__,
    url='http://anthony.land',
    packages=find_packages(),
    include_package_data=True,
    entry_points="""
        [console_scripts]
        clean=scripts.clean:clean
        db.createmigration=scripts.db:create_migration
        db.bootstrap=scripts.db:bootstrap_database
        db.drop=scripts.db:drop_database
        db.updateschema=scripts.db:update_schema
        serve=scripts.serve:serve
        setupenv=scripts:set_environment_vars
    """
)
