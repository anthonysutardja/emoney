#!/bin/sh

# Install python requirements
pip install -r requirements.txt;
pip install -r requirements-dev.txt;

# Add development commands and symlink project to env
python setup.py develop;
