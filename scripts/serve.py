from __future__ import absolute_import

import os

import click
from fabric.api import local

from scripts import set_environment_vars


@click.command()
def serve():
    # Set environment variables
    set_environment_vars()
    
    local("ipython -c 'from emoney import app; app.run()'")


if __name__ == '__main__':
    serve()
