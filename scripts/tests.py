from __future__ import absolute_import

import os

import click
from fabric.api import local


@click.command()
def tests():
    # TODO: Add environment
    command = 'py.test --tb=native {test_dir}'.format(
        test_dir=os.path.join(os.getcwd(), 'skeleton/tests'),
    )

    local(command)


if __name__ == '__main__':
    tests()
