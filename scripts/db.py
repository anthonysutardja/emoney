from __future__ import absolute_import

import os
import click
from fabric.api import local
from fabric.colors import green

from scripts import set_environment_vars


DB_USER = 'wells'
DB_PASS = 'fargo'
DB_NAME = 'emoney'


@click.command()
def update_schema():
    set_environment_vars()

    from emoney.services import Session
    from emoney.models.base import Model
    from emoney import models

    # Update schema
    session = Session()
    connectable = session.bind

    print green('Creating schema')
    Model.metadata.create_all(connectable)


@click.command()
def drop_database():
    local('mysql -uroot -e "DROP DATABASE IF EXISTS {db_name}";'.format(
        db_name=DB_NAME,
    ))


@click.command()
@click.pass_context
def bootstrap_database(ctx):
    # Create database
    local('mysql -uroot -e "CREATE DATABASE {db_name}";'.format(
        db_name=DB_NAME,
    ))

    # Grant privileges
    local(
        'mysql -uroot -e "GRANT ALL ON *.* TO \'{db_user}\'@\'loca'
        'lhost\' IDENTIFIED BY \'{db_pass}\';"'.format(
            db_name=DB_NAME,
            db_user=DB_USER,
            db_pass=DB_PASS,
        )
    )

    # Migrate it up
    ctx.invoke(migrate)


@click.command()
def migrate(action='upgrade', version='head'):
    set_environment_vars()
    local('alembic {action} {version}'.format(action=action, version=version))


@click.command()
@click.argument('message')
@click.pass_context
def create_migration(ctx, message):
    ctx.invoke(set_environment_vars)
    ctx.invoke(drop_database)
    ctx.invoke(bootstrap_database)

    local('alembic revision -m "{message}" --autogenerate'.format(
        message=message,
    ))


if __name__ == '__main__':
    pass
