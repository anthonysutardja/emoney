from __future__ import absolute_import

import os

import click
from fabric.api import local
from fabric.colors import green


@click.command()
def clean():
    # TODO: Add environment
    print green('Clean up .pyc files')
    local("find . -name '*.py[co]' -exec rm -f '{}' ';'")


if __name__ == '__main__':
    clean()
