import os


def set_environment_vars():
    # Set environment variables
    # Base config
    os.environ['SKELETON_BASE_CONFIG'] = os.path.join(
        os.getcwd(),
        'configs/base.cfg'
    )

    # Environment-specific config
    os.environ['SKELETON_CONFIG'] = os.path.join(
        os.getcwd(),
        'configs/development.cfg'
    )
