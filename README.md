# Emoney Service

## About
Stock tracker

## Development
How to develop with the workflow I've setup.

### Setup
Just run the following in a virtualenv:

    ./start.sh

This will pip install all the requirements into the env, and then it will run
`python setup.py develop` so you'll have special development commands.

| command             | Description                          | Parameters                   |
| ------------------- | ------------------------------------ | ---------------------------- |
| db.createmigration  | Create a new migration               | The revision message         |
| db.bootstrap        | Create and migrate db                | TBD                          |
| db.drop             | Drop the database                    | TBD                          |
| db.updateschema     | Update the schema without migration  | TBD                          |
| tests               | Runs all tests                       | TBD                          |
| serve               | Run the app locally                  | TBD                          |

To create the database, you'll need to run the following commands as `mysql -uroot`:

    CREATE USER 'wells'@'localhost' IDENTIFIED BY 'fargo';
    CREATE DATABASE emoney;
    GRANT ALL PRIVILEGES ON *.* TO 'wells'@'localhost' WITH GRANT OPTION;

To run the environment, you'll need to run `db.bootstrap` before proceeding.

### Tests
Tests are stored in `emoney/tests` and ran with `py.test`.
