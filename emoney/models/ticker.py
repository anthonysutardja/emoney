from __future__ import absolute_import

from sqlalchemy.schema import Column
from sqlalchemy.types import String, Integer

from emoney.models.base import Model


class Ticker(Model):

    __tablename__ = 'ticker'

    # Columns
    ticker_id = Column(Integer, primary_key=True)
    symbol = Column(String(8), unique=True, nullable=False)
    name = Column(String)

    # Relationships
    ticker_values = relationship('TickerValue')
