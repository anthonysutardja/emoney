# coding=utf-8
from __future__ import absolute_import

from sqlalchemy.ext.declarative import declarative_base


class Base(object):
    pass


Model = declarative_base(cls=Base)
