from __future__ import absolute_import

from sqlalchemy.orm import ForeignKey
from sqlalchemy.schema import Column
from sqlalchemy.types import Integer, DateTime, Float

from emoney.models.base import Model


class TickerValue(Model):

    __tablename__ = 'ticker_value'

    ticker_id = Column(
        Integer, ForeignKey('ticker.ticker_id'), primary_key=True)
    time = Column(DateTime, primary_key=True)
    close_price = Column(Float, nullable=False)
