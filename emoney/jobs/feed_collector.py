from __future__ import absolute_import

from emoney.lib.feeds.google import GoogleFeed
from emoney.services.ticker_service import TickerService


def main():
    tickers = TickerService.get_all_tickers()
    feed = GoogleFeed()

    for ticker in tickers:
        feed.get_prices(ticker.symbol)
        # TODO: Finish this!!


if __name__ == '__main__':
    main()
