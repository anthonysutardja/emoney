from flask import Flask


# The flask app object
app = Flask(__name__)


def initialize_app():
    # Initialize configs
    app.config.from_envvar('SKELETON_BASE_CONFIG')  # Inherit base
    app.config.from_envvar('SKELETON_CONFIG', silent=True)

    # Initialize views

    # NOTE: can register Flask blueprints here if we want to

    return app


initialize_app()
