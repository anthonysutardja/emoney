from __future__ import absolute_import

from sqlalchemy.exc import IntegrityError
from emoney.models import Ticker
from emoney.services import session


class TickerService(object):

    @classmethod
    def create_ticker(cls, ticker):
        """Create a new ticker."""
        session.add(ticker)
        session.commit()

    @classmethod
    def remove_ticker_by_ticker_id(cls, ticker_id):
        """Remove the ticker from the database."""
        ticker = cls.get_ticker_by_ticker_id(ticker_id)
        session.delete(ticker)
        session.commit()

    @classmethod
    def get_ticker_by_ticker_id(cls, ticker_id):
        """Return the ticker with the matching ticker_id."""
        ticker = session.query(Ticker).get(ticker_id)

        if not ticker:
            raise Exception('Ticker not found')
        return ticker

    @classmethod
    def get_all_tickers(cls):
        """Return all tickers in the database."""
        return session.query(Ticker).all()

    @classmethod
    def add_ticker_value_by_ticker(cls, ticker, time, close_price):
        """Add a new ticker value to the ticker."""
        # NOTE: this can become an issue with bulk inserts
        ticker_value = TickerValue(
            ticker_id=ticker.ticker_id,
            time=time,
            close_price=float(close_price),
        )

        try:
            session.add(ticker_value)
            session.commit()
        except IntegrityError as e:
            # Stock price for this value already inserted
            # Need some logging here
            raise e
