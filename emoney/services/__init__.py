from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from emoney import app


engine = create_engine(app.config.get('SQLALCHEMY_URL'))

Session = sessionmaker(bind=engine)
session = Session()
